package com.ruoyi.system.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.domain.BaseEntity;

import java.util.Date;

/**
 * 评价方案表 sys_evaluate_solution
 *
 * @author xiezhonggui
 */

public class SysEvaluateSolution extends BaseEntity {

    /**
     * 主键ID
     */
    private Long id;

    /**
     * 方案名称
     */
    private String name;

    /**
     * 方案执行日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date executingDate;

    /**
     * 部门Id
     */
    private String deptIds;

    /**
     * 管理人ID
     */
    private String managerIid;


    /**
     * 管理人名称
     */
    private String managerName;

    /**
     * 是否启用
     */
    private Integer enabled;

    /**
     * 是否删除
     */
    private Integer deleted;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getExecutingDate() {
        return executingDate;
    }

    public void setExecutingDate(Date executingDate) {
        this.executingDate = executingDate;
    }

    public String getDeptIds() {
        return deptIds;
    }

    public void setDeptIds(String deptIds) {
        this.deptIds = deptIds;
    }

    public String getManagerIid() {
        return managerIid;
    }

    public void setManagerIid(String managerIid) {
        this.managerIid = managerIid;
    }

    public String getManagerName() {
        return managerName;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    public Integer getEnabled() {
        return enabled;
    }

    public void setEnabled(Integer enabled) {
        this.enabled = enabled;
    }

    public Integer getDeleted() {
        return deleted;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }

    @Override
    public String toString() {
        return "SysEvaluateSolution{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", executingDate=" + executingDate +
                ", deptIds='" + deptIds + '\'' +
                ", managerIid='" + managerIid + '\'' +
                ", managerName='" + managerName + '\'' +
                ", enabled=" + enabled +
                ", deleted=" + deleted +
                '}';
    }
}
